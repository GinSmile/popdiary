﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace Diary
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class Index : Page
    {
        static  ObservableCollection<MyDiary> myIndex;
        static  Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        static Windows.Storage.ApplicationDataContainer container = localSettings.CreateContainer("diaryContainer", Windows.Storage.ApplicationDataCreateDisposition.Always);
  


        public Index()
        {
            this.InitializeComponent();
            NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
        }

        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。Parameter
        /// 属性通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                
                // Composite setting
                if (localSettings.Values["first"] == null)
                {
                    //  第一次进入，程序里面没有数据；
                    myIndex = new ObservableCollection<MyDiary>();
                    myIndex.Add(new MyDiary { Time = getNow(), Content = "欢迎使用Pop Diary（泡泡日记本）.您可以使用它来记录你的每一天。" });
                }
                else {
                    if (localSettings.Containers.ContainsKey("diaryContainer"))
                    {
                        myIndex = new ObservableCollection<MyDiary>();
                        int length = int.Parse(localSettings.Containers["diaryContainer"].Values["length"].ToString());
                        for (int i = 0; i < length; i++ )
                            myIndex.Add(new MyDiary { Time = localSettings.Containers["diaryContainer"].Values["time" + i].ToString(), 
                                Content = localSettings.Containers["diaryContainer"].Values["content" + i].ToString() }); 
                    }

                }                    
                
                IndexView.ItemsSource = myIndex;

            }
        }

  

 

        private void AddNewDiary(object sender, RoutedEventArgs e)
        {


            string now = getNow();

            MyDiary diary = new MyDiary { Time = now, Content = " " };
            myIndex.Insert(0, diary);
            
            storeData();
            
            this.Frame.Navigate(typeof(DiaryDetail), diary);

        }

        public static ObservableCollection<MyDiary> getCollection() {
            return myIndex;
        }


        private string getNow() {
            string now = DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + "  "
                 + DateTime.Now.Hour + "时" + DateTime.Now.Minute + "分" + "" + DateTime.Now.Second + "秒";
            return now;
        }

        private void Index_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Frame.Navigate(typeof(DiaryDetail), e.ClickedItem);
        }

        private void about_Click(object sender, RoutedEventArgs e)
        {
            
            this.Frame.Navigate(typeof(About));
        }



        private void userSetting_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UserSetting));
        } 

        internal bool EnsureUnsnapped()
        {
            // FilePicker APIs will not work if the application is in a snapped state.
            // If an app wants to show a FilePicker while snapped, it must attempt to unsnap first
            bool unsnapped = ((ApplicationView.Value != ApplicationViewState.Snapped) || ApplicationView.TryUnsnap());
            if (!unsnapped)
            {
                //do nothing
            }

            return unsnapped;
        }



        private async void export_Click(object sender, RoutedEventArgs e)
        {
            if(this.EnsureUnsnapped()){
                FileSavePicker savePicker = new FileSavePicker();
                savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                // Dropdown of file types the user can save the file as
                savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".txt" });
                // Default file name if the user does not type one in or select a file to replace
                savePicker.SuggestedFileName = "MyDiary";
                StorageFile file = await savePicker.PickSaveFileAsync();
                if (file != null)
                {
                    // Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdatesAsync.
                    CachedFileManager.DeferUpdates(file);
                    // write to file
                    string theAllDiary = "";
                    foreach (MyDiary s in myIndex) {
                        theAllDiary += s.Time + Environment.NewLine + s.Content + 
                            Environment.NewLine + Environment.NewLine;
                    }

                    await FileIO.WriteTextAsync(file, theAllDiary);
                    // Let Windows know that we're finished changing the file so the other app can update the remote version of the file.
                    // Completing updates may require Windows to ask for user input.
                    FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                    if (status == FileUpdateStatus.Complete)
                    {
                        MessageDialog md = new MessageDialog("保存成功");
                        await md.ShowAsync();
                    }
                    else
                    {
                        MessageDialog md = new MessageDialog("保存未成功");
                        await md.ShowAsync();
                    }
                }
                
            }
        }


        int indexNum = 0;
        private void remove_Click(object sender, RoutedEventArgs e)
        {

            myIndex.RemoveAt(indexNum);
            remove.Visibility = Windows.UI.Xaml.Visibility.Visible;
            removeAll.Visibility = Windows.UI.Xaml.Visibility.Visible;
            storeData();
        }

        private void select(object sender, SelectionChangedEventArgs e)
        {
           indexNum = IndexView.SelectedIndex;


           if (indexNum != -1)
           {
               remove.Visibility = Windows.UI.Xaml.Visibility.Visible;
               removeAll.Visibility = Windows.UI.Xaml.Visibility.Visible;
               myAppBar.IsOpen = true;
           }
           else {
               remove.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
               removeAll.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
           }
           

        }

        private async void removeAll_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog md = new MessageDialog("确定删除所有日记？");
            md.Commands.Add(new UICommand("确定", command =>
            {
                myIndex.Clear();

            }));
            md.Commands.Add(new UICommand("取消", command =>
            {
               //do nothing              
            }));

            await md.ShowAsync();

            storeData();

        }


        static public void storeData() {
            if (myIndex != null)
            {
                int length = myIndex.Count;
                localSettings.Containers["diaryContainer"].Values["length"] = length;
                for (int i = 0; i < length; i++)
                {
                    localSettings.Containers["diaryContainer"].Values["time" + i] = myIndex.ElementAt(i).Time;
                    localSettings.Containers["diaryContainer"].Values["content" + i] = myIndex.ElementAt(i).Content;

                }
                localSettings.Values["first"] = "false";
            }
        }



    }
}
