﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary
{
    public class MyDiary : INotifyPropertyChanged
    {
        private string _time;
        private string _content;
        public string Time
        {
            get
            {
                return _time;
            }
            set
            {
                _time = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Time"));
                }
            }
        }
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Content"));
                }
            }
        }

        public override string ToString()
        {
            return "Time:" + Time + "\nContent:" + Content;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
