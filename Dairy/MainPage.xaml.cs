﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.UserProfile;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace Diary
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {

        Windows.Storage.ApplicationDataContainer localSettings;

        public string currentPassword;

        public MainPage()
        {
            this.InitializeComponent();           

            localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            Object value = localSettings.Values["passwordData"];
            if (value == null)
            {
                
                currentPassword = "12345";
                tips.Text = "原始密码为" + currentPassword;
                localSettings.Values["passwordData"] = "12345";
            }
            else
            {
                tips.Text = "请输入密码";
                currentPassword = localSettings.Values["passwordData"].ToString();
            }


        }

      


        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。Parameter
        /// 属性通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        private void check_button(object sender, RoutedEventArgs e)
        {
            if (thePassBox.Password == currentPassword)
            {
                
                tips.Text = "密码正确";
                Frame.Navigate(typeof(Index));

            }
            else
            {
                tips.Text = "Wrong password, please input again.";
            }
        }

        private void userSetting_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(UserSetting));
        }

        private void about_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(About));
        }

     
    }
}
